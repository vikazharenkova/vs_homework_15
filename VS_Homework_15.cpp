﻿// VS_Homework_15.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

// Функция, выводящая все четные (EvenNumbers = true) или нечетные (EvenNumbers = false) числа от 0 до N
void EvenOddNumbers(bool EvenNumbers, int N)
{
    for (int i = 0; i <= N; ++i)
    {
        if (EvenNumbers == true and i % 2 == 0)
        {
            std::cout << i << " ";
        }
        else if (EvenNumbers == false and i % 2 != 0)
        {
            std::cout << i << " ";
        }
    }
}

int main()
{
    // Выводим все четные числа от 0 до N
    std::cout << "Outputting all even numbers from 0 to N: ";
    const int N(15);
    for (int i = 0; i <= N; ++i)
    {
        if(i%2==0)
        {
            std::cout << i << " ";
        }
    }
    std::cout << std::endl;

    // Выводим все четные числа от 0 до N,если NumberType = 0
    std::cout << "Outputting all even numbers from 0 to N: ";
    EvenOddNumbers(true, 18);
    std::cout << std::endl;

    // Выводим все нечетные числа от 0 до N,если NumberType = 1
    std::cout << "Outputting all odd numbers from 0 to N: ";
    EvenOddNumbers(false, 25);
    std::cout << std::endl;

    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
